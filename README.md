# README #

### This is a repository of 4 games ###

* **Bar Røv Dicegame** _(Morten)_
* **Guess a Number** _(Sophie)_
* **Hangman** _(Christine)_
* **Memory** _(Mikkel)_

### How do I get set up? ###

* Clone repo
* Open Index.html
* Play away

### Links for documentation (only if member of IBA-Kolding)###
* [Kravspec Bar Røv](https://365basyd.sharepoint.com/:w:/s/Section_11920/EUB1wA5LDflKuJ3mc1XMCi4BBT7yH8rkDRAz7DifTqSCjg?e=fdrZdZ)
* [Kravspec Guess a Number](https://365basyd.sharepoint.com/:w:/s/Section_11920/ERC6BkiuW8RNgOuvfdTaQroBYAS6EhBhKWCNDSu1ZWFggw?e=KN4tT5)
* [Kravspec Hangman](https://365basyd.sharepoint.com/:w:/s/Section_11920/EYAlASZswfNHjCHApcW3aYkBwWtzfn2ULJHXRmrZn_m0_A?e=aFg9ch)
* [Kravspec Memory](https://365basyd.sharepoint.com/:w:/s/Section_11920/EboV5boYjsRCm3VhoEPCygUB7Fd651GtzsCz_thBt4CjMg?e=eTpxSb)

* [Flowchart](https://www.figma.com/file/m5NjRRcK9TgFC0s4LjihOv/Flowchart?node-id=0%3A1)
* [Trello-Board](https://trello.com/b/BAB76BXt/projekt-uge-38-4-spil)
* [Presentation](https://365basyd.sharepoint.com/:p:/s/Section_11920/EdMK1shlg2FArVtsBQ4lww8BeBPjbs72wwPTaCyYeAYyxw?e=WEscAq)