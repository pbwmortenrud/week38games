//THIS IS A CODE PLAYGROUND FOR WHEN HELPING OTHER TEAMMEMBERS


//Get element in array and do something
let keyPressed = "b";
let myWord = ["a", "b", "c", "b"];
console.log(keyPressed);

function lookupCharInWord() {
  if (myWord.includes(keyPressed)) {
    console.log(keyPressed + " was in: " + myWord);
  } else console.error(keyPressed + " wasn't found in " + myWord);

  console.log(myWord.indexOf(keyPressed));

  for (let i = 0; i < myWord.length; i++) {
    if (myWord[i] == keyPressed) {
      //Do everything you need
    }
  }
}

lookupCharInWord();


//HIGHSCORE CODE SAVED IN LOCAL STORAGE

const NO_OF_HIGH_SCORES = 10;
const HIGH_SCORES = "highScores";
const highScoreString = localStorage.getItem(HIGH_SCORES);
const highScores = JSON.parse(highScoreString) ?? [];

function checkHighScore(score) {
  const highScores = JSON.parse(localStorage.getItem(HIGH_SCORES)) ?? [];
  const lowestScore = highScores[NO_OF_HIGH_SCORES - 1]?.score ?? 0;

  if (score > lowestScore) {
    saveHighScore(score, highScores);
    showHighScores();
  }
}

function gameOver() {
  // Other game over logic.
  checkHighScore(players[0].score); //INPUT CURRENT PLAYER SCORE
}

function saveHighScore(score, highScores) {
  const name = prompt("You got a highscore! Enter name:");
  const newScore = { score, name };

  // 1. Add to list
  highScores.push(newScore);

  // 2. Sort the list
  highScores.sort((a, b) => b.score - a.score);

  // 3. Select new list
  highScores.splice(NO_OF_HIGH_SCORES);

  // 4. Save to local storage
  localStorage.setItem(HIGH_SCORES, JSON.stringify(highScores));
}

function showHighScores() {
  const highScores = JSON.parse(localStorage.getItem(HIGH_SCORES)) ?? [];
  const highScoreList = document.getElementById(HIGH_SCORES);

  highScoreList.innerHTML = highScores
    .map((score) => `<li>${score.score} - ${score.name}`)
    .join("");
}