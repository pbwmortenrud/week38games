//helper function
const $ = function (foo) {
  return document.getElementById(foo);
};
const $c = function (foo) {
  return document.getElementsByClassName(foo);
};

class Die {
  constructor(ID = 0, numberOfEyes = 6, currentEye, isLocked = false) {
    this.ID = ID;
    this.currentEye = currentEye;
    this.numberOfEyes = numberOfEyes;
    this.isLocked = isLocked;
    if (!currentEye) {
      console.error("currentEye not set");
      this.currentEye = Math.floor(Math.random() * this.numberOfEyes + 1);
    }
  }
  lock() {
    this.isLocked = this.isLocked ? false : true; //mutator
  }
  unlock() {
    this.isLocked = false; //mutator
  }
  getLocked() {
    return this.isLocked; //accessor
  }
  setCurrentEye(x) {
    if (0 < x <= this.numberOfEyes) this.currentEye = x;
    else
      console.error(
        "Current eyes must be within max number of eyes on the die"
      );
  }
  toString() {
    return `[d${this.ID}], [${this.currentEye}], Locked:${this.getLocked()}\n`;
  }

  /**
   * @param {int=} number - any given number gets locked. Multiple numbers seperated with "," is allowed.
   */
  //ROLLS THE DIE, AND RETURNS THE NUMBERS IF NOT LOCKED
  roll() {
    let tmpSum = 0;
    //If die is locked, it doesn´t roll
    if (this.getLocked()) {
      console.error(`[d${this.ID}] is locked`);
      return tmpSum;
    } else {
      this.currentEye = Math.floor(Math.random() * this.numberOfEyes + 1); //roll
      tmpSum = this.currentEye;
      //This locks the die ONLY if any arguments given matches the eyenumber
      for (let i = 0; i < arguments.length; i++) {
        if (this.currentEye == arguments[i]) {
          this.lock();
          console.log(`Locking [d${this.ID}]`);
          tmpSum = -1;
        }
      }
      return tmpSum;
    }
  }
} //END OF CLASS [DIE]

class Player {
  constructor(
    ID,
    name = "John Doe",
    score = 0,
    totalScore = 0,
    scores = [],
    highScore = 0
  ) {
    this.ID = ID;
    this.name = name;
    this.score = score;
    this.totalScore = totalScore;
    this.scores = scores;
    this.highScore = highScore;
  }
  saveScore() {
    this.scores.push(this.score);
    this.totalScore += this.score;
    this.score = 0;
  }
} //END OF CLASS [PLAYER]

//PLAYERS FUNCTIONS
let players = [];
let currentPlayer = new Player(0, "John Doe");

function initPlayer(name) {
  players.push(new Player(players.length + 1, name));
}

function setCurrentPlayer(index) {
  currentPlayer = players[index - 1];
}

function changePlayer() {
  if (currentPlayer.ID != players.length) {
    setCurrentPlayer(currentPlayer.ID + 1);
  } else if (isLastRound) {
    gameOver();
  } else {
    setCurrentPlayer(1);
  }

  roundSum = 0;
}

function removePlayer(index) {
  players.splice(index - 1, 1);
}

function checkWinner() {
  let winner = players[0];
  players.forEach((player) => {
    if (player.totalScore > winner.totalScore) {
      winner = player;
    }
  });
  return winner;
}

//GAME VARIABLES
let roundSum = 0;
let rollSum = 0;
let totalSum = 0;
let maxSum = 100;
let isLastRound = false;
let dice = initDice(6, 6); //Initialize 6 dice with 6 eyes

//GAME FUNCTIONS
function initGame() {
  newGame(
    Number(prompt("Hvor mange spillere (2-4)", "2")),
    Number(prompt("Hvor meget spiller I til? (50-1000)", "100"))
  );
  $("btn-rollDice").style.visibility = "visible";
  drawHighScores();
}

function newGame(numberOfPlayers = 2, maxPoints = 100) {
  isLastRound = false;
  resetDice(dice);
  resetScore();
  setMessage("Rul med terningerne");
  players = [];
  for (let i = 0; i < numberOfPlayers; i++) {
    initPlayer(prompt("Indtast ny spiller", "John Doe"));
  }
  setMaxSum(maxPoints);
  setCurrentPlayer(1);
  drawPlayers();
}

function setMaxSum(sum) {
  maxSum = sum;
}

function checkMaxScore() {
  if (currentPlayer.totalScore >= maxSum) {
    console.log("Max sum er nået og spillet skal afsluttes");
    isLastRound = true;
  }
}

function gameOver() {
  console.log("Game Over");
  const winner = checkWinner();
  setMessage(
    `Spillet er slut. ${winner.name} vinder spillet med ${winner.totalScore} points`
  );
  btnRoll.style.visibility = "hidden";
  saveHighScores(players);
  drawHighScores();
}

//HIGHSCORE FUNCTIONS
// Inspired by: https://michael-karen.medium.com/how-to-save-high-scores-in-local-storage-7860baca9d68
const NO_OF_HIGH_SCORES = 20;
const HIGH_SCORES = "highScores";
const highScoreString = localStorage.getItem(HIGH_SCORES);
const highScores = JSON.parse(highScoreString) ?? []; //if not exists in LocalStorage, return an empty array

function checkHighScores(score) {
  const highScores = JSON.parse(localStorage.getItem(HIGH_SCORES)) ?? [];
  const lowestScore = highScores[NO_OF_HIGH_SCORES - 1]?.score ?? 0;
  if (score > lowestScore) {
    return true;
  } else return false;
}
function saveHighScores(players) {
  const highScores = JSON.parse(localStorage.getItem(HIGH_SCORES)) ?? [];
  players.forEach((player, i) => {
    let name = player.name;
    let score = player.totalScore;
    const newScore = { name, score };
    //Check if score is high enough for the Highscore list
    if (checkHighScores(player.totalScore)) {
      // 1. Add to list
      highScores.push(newScore);
      // 2. Sort the list
      highScores.sort((a, b) => b.score - a.score);
      // 3. Select new list
      highScores.splice(NO_OF_HIGH_SCORES);
      // 4. Save to local storage
      localStorage.setItem(HIGH_SCORES, JSON.stringify(highScores));
    }
  });
}

//PLAY GAME

//Initiating dice and rolls them once to show them. Doesn't lock any yet
function initDice(numberOfDice, eyes) {
  const diceArr = [];
  for (let i = 0; i < numberOfDice; i++) {
    diceArr.push(new Die(i + 1, eyes));
  }
  console.log("%c initiating dice", "color: #FF00FF");
  console.log("%c" + diceArr + "", "color: #bada55");
  return diceArr;
}

function resetDice(dice = []) {
  dice.forEach((die, index) => {
    die.unlock();
  });
  removeDice();
}

function newRound() {
  checkMaxScore();
  changePlayer();
  resetScore();
  resetDice(dice);
  drawPlayers();
}

function isBareAss(dice) {
  let bareAss = true;
  for (let i = 0; i < dice.length; i++) {
    if (!dice[i].getLocked()) {
      bareAss = false;
      return;
    }
  }
  if (bareAss) {
    console.log("Bar Røv");
  }
  return bareAss;
}

//Roll all the dice in a dice Array. If it is locked, it won't roll, and return 0
function rollDice(dice = []) {
  let tmpSum = 0;
  let isRollValid = true;

  if (isBareAss(dice)) {
    //Dont roll, but end round
    currentPlayer.saveScore();
    console.log(
      `Round sum was: ${roundSum}, and added to ${currentPlayer.name}'s score'`
    );
    drawBareAss();
    newRound();
  } else {
    //console.log("%c rolling.......", "color: #005FFA");
    dice.forEach((die) => {
      let points = die.roll(2, 5); //rolls die, and returns the number. If the parameters given are rolled, then return 0.

      if (points == -1) {
        isRollValid = false;
      } else tmpSum += points;
    });

    if (!isRollValid) {
      tmpSum = 0;
      setMessage("Terning(er) låst. Slaget tæller ikke");
    } else {
      roundSum += tmpSum;
      currentPlayer.score += tmpSum;
      setMessage("Sum gemt. Rul igen");
    }
    //CONSOLE OUTPUT------------------------------------
    console.log(dice.toString());
    console.log("Throw gives points? " + isRollValid);
    console.log(`Roll sum: ${tmpSum}`);
    console.log(`Round sum: ${roundSum}`);
    //--------------------------------------------------

    drawScore(tmpSum, roundSum);
    drawPlayers();
    drawDice(dice);
  }
}

//DOM FUNCTIONS
/*Drawing of dice were inspired of: https://www.geeksforgeeks.org/building-a-dice-game-using-javascript/
However, the dynamic setting of dice-#.PNG to the div was too heavy, so I decided to borrow the PNGs, 
and set them as BG in each own class*/
let btnTest = document.getElementById("btn-new");
let btnNew = $("btn-new");
let btnRoll = $("btn-rollDice");
let btnNextPlayer = $("btn-nextPlayer");

function drawScore(tmpSum, roundSum) {
  $("currentScore").innerText = " " + tmpSum;
  $("roundScore").innerText = " " + roundSum;
}

function resetScore() {
  $("currentScore").innerText = "";
  $("roundScore").innerText = "";
}

function drawHighScores() {
  const highScores = JSON.parse(localStorage.getItem(HIGH_SCORES)) ?? [];
  const highScoreName = $("highscoreName");
  const highScoreScore = $("highscoreScore");

  highScoreName.innerText = highScores
    .map((score) => `${score.name}`)
    .join("\n");
  highScoreScore.innerText = highScores
    .map((score) => `${score.score}`)
    .join("\n");
}

function drawPlayers() {
  $("names").innerText = "";
  $("scores").innerText = "";
  $("currentPlayer").innerText = currentPlayer.name;
  players.forEach((player, index) => {
    //Drawing players to scoreboard
    $("names").innerText += player.name + "\n";
    $("scores").innerText += player.totalScore + "\n";
  });
}

function drawDice(dice = []) {
  removeDice();
  const myNode = $("dice");
  dice.forEach((die, index) => {
    let dieNumber = die.currentEye;
    let newDie = document.createElement("div");
    newDie.id = `die-${index}`;
    newDie.classList.add("die", `die-${dieNumber}`);

    myNode.appendChild(newDie);

    if (die.getLocked()) {
      newDie.classList.toggle("rotate");
      newDie.classList.add("overlay");
    } else newDie.classList.add("rotate");
  });
}

function removeDice() {
  const myNode = $("dice");
  while (myNode.lastElementChild) {
    myNode.removeChild(myNode.lastElementChild);
  }
}

function drawBareAss() {
  let message = "Bar Røv! Turen går videre.";
  setMessage(message);
}

function setMessage(string) {
  let messageElm = $("message");
  messageElm.innerText = string;
}
function animateDie(id) {
  const die = $("die-" + id);
  die.classList.toggle("rotate");
}

//TEST CODE

//-----------PLAY ------------------------------

btnNew.addEventListener("click", function () {
  initGame();
});
btnRoll.addEventListener("click", function () {
  rollDice(dice);
});

drawHighScores();
