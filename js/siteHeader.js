function addHeader() {
    
    let header = document.createElement("header");
    let nav = document.createElement("nav");
    let a = document.createElement("a");
    a.classList.add("button");

    a.href="index.html";
    a.innerText = "Forside";

    nav.appendChild(a);
    header.appendChild(nav);
    document.body.insertBefore(header, document.body.firstChild);
}
 document.addEventListener("load", addHeader());   
