let userInput = prompt('Hvor mange brikker vil du gerne spille med?', 'Skriv et lige antal');
let card;
let cardInner;
let cardInnerFront;
let cardInnerBack;

const initGame = () => {
    // Tag imod brugerens input om hvor mannge brikker spilleren ønsker
    const count = userInput;
    // Udregning til at konstruere et grid med brikkerne, som bruges når makeGrid bliver callet
    let pieces = userInput / 10;

    // Opbygger et array med antallet af brikker divideret med 2 for at få et lige antal par
    const randomNumbers = Array.from(new Array(count / 2))
        // Opbyg et array med numre i par - e.g. [1, 1, 2, 2, 3, 3, 4, 4, ...]
        .reduce((acc, _, index) => acc.concat([index, index]), [])
        // Tilfældiggør brikkerne - e.g. [1, 3, 2, 4, 1, 4, 2, 3, ...]
        .map((value) => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);

    console.log(randomNumbers);

    // Sætter en variabel for beholderen til alle brikkerne
    const container = document.getElementById("VS-container");

    // Funktionen der generere alle brikkerne
    function makeGrid(rows, cols, number) {
        let i = 0;
        // container.style.setProperty('--grid-rows', rows);
        // container.style.setProperty('--grid-cols', cols);
        for (c = 0; c < (rows * cols); c++) {
            card = document.createElement("div");
            cardInner = document.createElement("div");
            cardInnerFront = document.createElement("div");
            cardInnerBack = document.createElement("div");


            img = document.createElement("img");
            cardInnerBack.appendChild(img).className = "VS-grid-item-img ";
            img.src = `https://picsum.photos/id/${number[i] + 20}/100/100`;
            i++;

            // Sætter en class for det forskellige kasser i kort kassen
            container.appendChild(card).className = "VS-grid-item " + `${number[i - 1] + 20}`;
            card.appendChild(cardInner).className = "VS-card-inner";
            card.appendChild(cardInnerFront).className = "VS-card-inner-front";
            card.appendChild(cardInnerBack).className = "VS-card-inner-back";

            card.appendChild(cardInner);
            cardInner.appendChild(cardInnerFront);
            cardInner.appendChild(cardInnerBack);
        };
    };
    
    // Test på at tildele hver brik et billede fra en ekstern hjemmeside
    randomNumbers.forEach((number) => {
        // Hver brik får et tilfældigt billede fra en ekstern hjemmeside, for at tjekke om tallene passer
        // + 20 er for at få nogle billeder fra hjemmesiden, der ikke er for ens
        const imageUrl = `https://picsum.photos/id/${number + 20}/100/100`;

        console.log(imageUrl);
    });

    function pickCard(number) {
        document.querySelectorAll('.VS-grid-item').forEach((item, index) => {
            item.addEventListener('click', (e) => {
                item.firstChild.classList.toggle('VS-grid-item-clicked');
                console.log(number[index] + 20);
                startTimer();
            });
        });
    };
    
    makeGrid(pieces, 10, randomNumbers);
    pickCard(randomNumbers);

    //game timer
    let second = 0, minute = 0;
    let timer = document.querySelector(".VS-timer");
    let interval;
    function startTimer(){
        startTimer = function(){};
        interval = setInterval(function(){
            timer.innerHTML = "Din tid: " + minute +"m " + second + "s";
            second++;
            if(second == 60){
                minute++;
                second = 0;
            }
        }, 1000);
    }
};

window.addEventListener("load", initGame);