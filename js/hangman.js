// Hangman

"use strict";

// List of words to guess
let wordList = [
	"boolsk",
	"browser",
	"brugeroplevelser",
	"brugerundersøgelser",
	"brugervenlighed",
	"css",
	"elementer",
	"hjemmesideudvikling",
	"html",
	"javascript",
	"javascriptfunktion",
	"kardinalitet",
	"konkatenering",
	"medieansvarlig",
	"moduler",
	"objekter",
	"operator",
	"professionsbachelor",
	"pseudokode",
	"rekursion",
	"sandhedstabeller",
	"server",
	"sql",
	"syntaks",
	"udviklingsmiljøer",
	"udviklingsværktøjer",
	"validering",
	"webprogrammering"
];

// Amount of lives available
let lives = 6;

// Count the amount of wrong guesses
let wrongGuesses = 0;

// Letters the user has guessed wrong
let lettersWrong = [];

// Randomly select the word the user has to guess from the list of words
let wordInPlay = wordList[Math.floor(Math.random() * wordList.length)];
console.log(wordInPlay);

// Make a copy of the word
let wordOriginal = wordInPlay;

// Replace the letters in the word with underscores
wordInPlay = wordInPlay.replace(/[a-å]/gi, "_");

// Seperate the letters for each word in an array
let lettersHidden = wordInPlay.split("");
let lettersShown = wordOriginal.split("");

// Create list items for each letter in the word and add them to ul
let word = document.getElementById("word");
lettersHidden.forEach((item)=> {
	let li = document.createElement("li");
	li.innerText = item;
	li.classList.add("letter");
	word.appendChild(li);
});

// Check if the letters the user guesses are in the word
document.addEventListener("keydown", function (event) {
	let eKey = event.key.toLowerCase(); //no difference between lower and upper case
	let eCodeKey = ( //restrict to letters only
		event.keyCode >= 65 && //a
		event.keyCode <= 90 || //z
		event.keyCode === 192 || //æ
		event.keyCode === 221 || //å
		event.keyCode === 222 //ø
	);
	for (let i = 0; i < lettersShown.length; i++) {
		// Show the right letters
		if (eKey === lettersShown[i] && eCodeKey) {
			wordInPlay =
				wordInPlay.substring(0, i) +
				lettersShown[i] +
				wordInPlay.substring(i + 1);
			console.log(wordInPlay);
			showLetters(wordInPlay);
		}
	}
	// Add the wrong letters to an array
	if (eKey !== lettersShown[lettersShown.indexOf(eKey)] &&
		lettersWrong.indexOf(eKey) == -1 && eCodeKey) {
		lettersWrong.push(eKey);
		// Create list items for each wrong letter and add them to ul
		let wrongs = document.getElementById("wrongs");
		wrongs.innerHTML = ""; //deletes the list, before adding again
		lettersWrong.forEach((item) => {
			let li = document.createElement("li");
			li.innerText = item;
			li.classList.add("wrong-letter");
			wrongs.appendChild(li);
		});
		console.log(lettersWrong);
		// Count wrong guesses
		wrongGuesses++;
		console.log(wrongGuesses);
		if (wrongGuesses < lives)
			gameOver("livesLeft");
		else if (wrongGuesses => lives)
			gameOver("lose");
	}
});

// Show the right letters in the word
function showLetters(wordInPlay) {
	lettersHidden = wordInPlay.split("");
	for (let i = 0; i < lettersHidden.length; i++) {
		word.querySelectorAll("li")[i].innerText = lettersHidden[i];
		document.getElementById("text").innerHTML = "Du fandt et bogstav!";
		if (wordInPlay === wordOriginal) {
			gameOver("win");
		}
	}
}

// When the user wants to guess the word
let btnGuess = document.getElementById("btn-guess");
btnGuess.onclick = function () {
	let guess = window.prompt("Gæt ordet").toLowerCase();
	// If the user guesses the word
	if (guess === wordOriginal) {
		showLetters(wordOriginal);
	}
	// If the user doesn't guess the word
	else if (guess !== wordOriginal) {
		// Count wrong guesses
		wrongGuesses++;
		console.log(wrongGuesses);
		if (wrongGuesses < lives) 
			gameOver("livesLeft");
		else if (wrongGuesses => lives)
			gameOver("lose");
	}
};

// The game is over
function gameOver(string) {
	if (string === "lose") {
		document.getElementById("text").innerHTML = "Beklager! Du har tabt!";
		word.style.color = "#F3016B";
	}
	if (string === "win"){
		document.getElementById("text").innerHTML = "Tillykke! Du har vundet!";
		word.style.color = "#76B900";
	}
	if (string === "livesLeft"){
		document.getElementById("text").innerHTML = "Du har " + (lives - wrongGuesses) + " liv tilbage";
	}
}

// If the user wants to start a new game
let btnNew = document.getElementById("btn-new");
btnNew.onclick = function () {
	location.reload();
};